package alexandrov.rentcar.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddClientRequest {
    private String name;
    private int birthyear;
    private String carBrand;
    private int carCreationYear;
}

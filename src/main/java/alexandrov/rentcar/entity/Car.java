package alexandrov.rentcar.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "creation_year")
    private int creationYear;

    @OneToOne(targetEntity = Client.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private Client client;
}

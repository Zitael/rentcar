package alexandrov.rentcar.controllers;

import alexandrov.rentcar.entity.Car;
import alexandrov.rentcar.entity.Client;
import alexandrov.rentcar.json.request.RemoveClientRequest;
import alexandrov.rentcar.repository.CarRepository;
import alexandrov.rentcar.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RemoveController {
    private final CarRepository carRepository;
    private final ClientRepository clientRepository;

    @PostMapping("/api/remove")
    public ResponseEntity<Void> removeClient(@RequestBody RemoveClientRequest request) {
        Client client = clientRepository.findByNameIsLike(request.getName());
        if (client == null) {
            log.error("Client not found name {}", request.getName());
            return badRequest().build();
        }
        Car car = carRepository.findByClientIs(client);
        if (car == null) {
            log.error("Car not found by brand {} and client {}", request.getCarBrand(), request.getName());
            return notFound().build();
        }
        car.setClient(null);
        carRepository.save(car);
        clientRepository.delete(client);
        log.info("Removed client {} with car {}", client.getName(), car.getBrand());
        return ok().build();
    }
}

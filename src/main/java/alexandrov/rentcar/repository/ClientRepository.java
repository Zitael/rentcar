package alexandrov.rentcar.repository;

import alexandrov.rentcar.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByNameIsLikeAndBirthyearIs(String name, int birthyear);
    Client findByNameIsLike(String name);
}

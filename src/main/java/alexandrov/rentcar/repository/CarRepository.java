package alexandrov.rentcar.repository;

import alexandrov.rentcar.entity.Car;
import alexandrov.rentcar.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
    Car findByClientIs(Client client);
    Car findByBrandIsLikeAndCreationYearIs(String brand, int creationYear);
}

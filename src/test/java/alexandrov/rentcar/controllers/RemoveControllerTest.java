package alexandrov.rentcar.controllers;

import alexandrov.rentcar.json.request.RemoveClientRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class RemoveControllerTest {
    private MockMvc mockMvc;
    private ObjectMapper om = new ObjectMapper();
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void removeClient_whenClientNotFound() throws Exception {
        RemoveClientRequest removeClientRequest = new RemoveClientRequest("Vasiliy", "BMW");
        mockMvc.perform(
                post("/api/remove")
                        .content(om.writeValueAsString(removeClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void removeClient_whenCarNotFound() throws Exception {
        RemoveClientRequest removeClientRequest = new RemoveClientRequest("Vladimir", "Audi");
        mockMvc.perform(
                post("/api/remove")
                        .content(om.writeValueAsString(removeClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void removeClient_successRemoved() throws Exception {
        RemoveClientRequest removeClientRequest = new RemoveClientRequest("Ivan", "BMW");
        mockMvc.perform(
                post("/api/remove")
                        .content(om.writeValueAsString(removeClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }


}
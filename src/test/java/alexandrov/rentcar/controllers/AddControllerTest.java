package alexandrov.rentcar.controllers;

import alexandrov.rentcar.json.request.AddClientRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AddControllerTest {
    private MockMvc mockMvc;
    private ObjectMapper om = new ObjectMapper();
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void addClient_whenClientExists() throws Exception {
        AddClientRequest addClientRequest = new AddClientRequest("Ivan", 1990, "Porsche", 2015);
        mockMvc.perform(
                post("/api/add")
                        .content(om.writeValueAsString(addClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void addClient_whenCarNotFound() throws Exception {
        AddClientRequest addClientRequest = new AddClientRequest("Vladimir", 1990, "Audi", 2015);
        mockMvc.perform(
                post("/api/add")
                        .content(om.writeValueAsString(addClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addClient_successClientAdded() throws Exception {
        AddClientRequest addClientRequest = new AddClientRequest("Maria", 1980, "Porsche", 2015);
        mockMvc.perform(
                post("/api/add")
                        .content(om.writeValueAsString(addClientRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}